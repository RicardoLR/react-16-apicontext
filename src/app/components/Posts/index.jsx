/* @flow */
// Dependencies
import React, { Component, Fragment } from 'react'
// Components
import UserInfo from 'Components/Users/Info'
// Contexts
import { BlogConsumer } from 'Context/Blog'
// Styles
import styles from './Posts.less'

// Flow Props
type Props = {
  /** */
}

/**
 * Para esta clase la envolvimos con el Provider
 * 
 * 
 * <BlogProvider value={{ posts, userData, users }}>

    <Posts {...this.props} />

  </BlogProvider>

 */
class Posts extends Component<Props> {

  static contextType = BlogConsumer

  render() {

    /**
     * TOMA el Context implitamente
     */
    const blogContext = this.context

    const userContext = blogContext.userData
    const postsContext = blogContext.posts

    return (
      <Fragment>
        <div className={styles.posts}>
          <div className={styles.header}>
            <h1>Blog</h1>
          </div>

          <UserInfo userInfo={userContext} />

          <div>
            {postsContext &&
              postsContext.map(post => (
                <div key={post.id} className={styles.posts}>
                  <h2>{post.title}</h2>
                  <p>{post.body}</p>
                </div>
              ))}
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Posts
