/* @flow */
import React from 'react'
// Dependendicies
import styles from './Users.scss'

// Flow Props
type Props = {
  getUser: mixed
}

function UserForm(props: Props) {
  const { getUser } = props

  return (

    <div>
      <form onSubmit={getUser}>
        {/** Para enviar en el sig button el evento de input, hacer un hadlerTextInput */}
        <input type="text" name="username" className={styles.input} />
        <button type="submit">Submit</button>
      </form>

      {/* <button onClick={getUser('Luis')}>
        Hacer peticion por button aparte
      </button> */}

    </div>
      

  )
}

export default UserForm
